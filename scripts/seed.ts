import { PrismaClient } from '@prisma/client';
import { addWeeks } from 'date-fns';
const prisma = new PrismaClient();

const setting = await prisma.settings.upsert({
	where: {
		id: '_'
	},
	create: {
		id: '_',
		inscriptionsCloseAt: addWeeks(new Date(), 1),
		inscriptionsOpenAt: new Date()
	},
	update: {
		inscriptionsCloseAt: addWeeks(new Date(), 1),
		inscriptionsOpenAt: new Date()
	}
});
console.log(`Created setting ${JSON.stringify(setting)}`);
