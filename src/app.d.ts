// See https://kit.svelte.dev/docs/types#app

import type { PrismaClient } from '@prisma/client';

// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			user?: null | (DatabaseUserAttributes & { id: string });
			session?: null | (DatabaseSessionAttributes & { id: string });
		}
		// interface PageData {}
		// interface PageState {}
		// interface Platform {}
	}

	let prisma: PrismaClient;
}

interface DatabaseUserAttributes {
	// username: string;
	email: string;
	churrosId: string;
	admin: boolean;
	dead: boolean;
	name: string;
	churrosUid: string;
	pictureURL: string;
}

interface DatabaseSessionAttributes {
	churrosToken: string;
}

declare module 'lucia' {
	interface Register {
		Lucia: typeof lucia;
		DatabaseUserAttributes: DatabaseUserAttributes;
		DatabaseSessionAttributes: DatabaseSessionAttributes;
	}
}

export {};
