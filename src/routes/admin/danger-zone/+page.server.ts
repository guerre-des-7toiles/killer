import { randomSort } from '$lib/random';
import { error } from '@sveltejs/kit';
import prisma from '$lib/server/prisma';

export const actions = {
	async unassignAll({ locals }) {
		if (!locals.user?.admin) {
			error(403, {
				message: "Tu n'es pas administrateur·ice"
			});
		}

		await prisma.$executeRaw`TRUNCATE TABLE _wantsToKill`;
	},

	async randomizeAssignments({ locals }) {
		if (!locals.user?.admin) {
			error(403, {
				message: 'Seuls les administrateur·ice·s'
			});
		}

		await prisma.$executeRaw`TRUNCATE TABLE _wantsToKill`;

		const survivors = await prisma.user.findMany({
			where: { dead: false }
		});

		randomSort(survivors);

		console.log(`Randomizing assignments for ${survivors.length} survivors`);

		for (let i = 0; i < survivors.length; i++) {
			const survivor = survivors[i];
			const nextSurvivor = survivors[(i + 1) % survivors.length];
			console.log(`${survivor.churrosUid} -> ${nextSurvivor.churrosUid}`);
			await prisma.user.update({
				where: { id: survivor.id },
				data: {
					wantsToKill: {
						connect: { id: nextSurvivor.id }
					}
				}
			});
		}
	}
};
