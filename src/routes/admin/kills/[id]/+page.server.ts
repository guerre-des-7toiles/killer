import prisma from '$lib/server/prisma.js';

export async function load({ params: { id } }) {
	return {
		kill: await prisma.kill.findUniqueOrThrow({
			where: {
				id
			},
			include: {
				murderer: true,
				victim: true,
				judge: true
			}
		})
	};
}
