import { currentWave } from '$lib/server/db';
import prisma from '$lib/server/prisma';

export async function load() {
	const wave = await currentWave({});
	return {
		wave,
		kills: await prisma.kill.findMany({
			where: {
				OR: [{ waveId: wave?.id }, { verdict: null }]
			},
			include: {
				murderer: true,
				victim: true,
				judge: true
			},
			orderBy: [{ verifiedAt: 'desc' }, { submittedAt: 'desc' }]
		})
	};
}
