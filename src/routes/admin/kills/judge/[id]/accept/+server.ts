import { updateUserKilledStatus } from '$lib/server/db.js';
import prisma from '$lib/server/prisma.js';
import { error, json } from '@sveltejs/kit';

export async function POST({ params: { id }, locals: { user } }) {
	if (!user?.admin) throw error(403, { message: "Tu n'es pas administrateur·ice !" });
	
	const kill = await prisma.kill.update({
		where: {
			id
		},
		data: {
			judge: {
				connect: { id: user.id }
			},
            verifiedAt: new Date(),
            verdict: true
		}
	});
	await updateUserKilledStatus(kill.victimId);
	return json({ status: 'ok' });
}
