import prisma from '$lib/server/prisma';
import { error } from '@sveltejs/kit';

export async function load({ params }) {
	const wave = await prisma.wave.findUnique({
		where: {
			id: params.id
		}
	});

	if (!wave) {
		error(404, { message: 'Vague non trouvée' });
	}

	return {
		wave,
		kills: prisma.kill.findMany({
			where: {
				waveId: wave.id
			},
			include: {
				murderer: true,
				victim: true,
				judge: true
			},
			orderBy: [{ verifiedAt: 'desc' }, { submittedAt: 'desc' }]
		})
	};
}
