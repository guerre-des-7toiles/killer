import { createDateTime } from '$lib/dates.js';
import * as schemas from '$lib/schemas.js';
import { saveToStorage } from '$lib/server/files.js';
import prisma from '$lib/server/prisma';
import { error, fail } from '@sveltejs/kit';
import path from 'node:path/posix';
import { superValidate } from 'sveltekit-superforms/server';

export async function load() {
	const createWaveForm = await superValidate(schemas.createWave);
	const wave = await prisma.wave.findFirst({
		where: {
			startsAt: {
				lte: new Date()
			},
			endsAt: {
				gte: new Date()
			}
		}
	});

	if (!wave) {
		fail(404, { message: 'Aucune vague en cours' });
	}

	return {
		createWaveForm,
		wave,
		killsToJudgeCount: await prisma.kill.count({
			where: {
				waveId: wave?.id,
				verdict: null
			}
		}),
		survivors: await prisma.user.findMany({
			where: {
				dead: false
			},
			include: {
				wantedBy: { where: { dead: false } },
				wantsToKill: { where: { dead: false } }
			},
			orderBy: {
				name: 'asc'
			}
		})
	};
}

export const actions = {
	async createWave({ request, locals }) {
		if (!locals.user?.admin) {
			error(403, { message: "Tu n'es pas administrateur·ice !" });
		}

		const formData = await request.formData();
		const newWaveForm = await superValidate(formData, schemas.createWave);

		if (!newWaveForm.valid) {
			return fail(400, { newWaveForm });
		}

		const { immunityObjects } = await prisma.wave.create({
			data: {
				startsAt: createDateTime(
					newWaveForm.data.date,
					newWaveForm.data.startTime,
					newWaveForm.data.timezone
				),
				endsAt: createDateTime(
					newWaveForm.data.date,
					newWaveForm.data.endTime,
					newWaveForm.data.timezone
				),
				name: newWaveForm.data.name,
				immunityObjects: {
					create: newWaveForm.data.objectName
						? [
								{
									name: newWaveForm.data.objectName,
									pictureFile: ''
								}
							]
						: []
				}
			},
			include: {
				immunityObjects: true
			}
		});

		if (immunityObjects.length > 0) {
			const { id } = immunityObjects[0];
			const file = formData.get('objectPicture');
			if (file instanceof File) {
				// Create file in /storage/objects/
				const filename = await saveToStorage(
					`objects/${id}${path.extname(file.name)}`,
					await file.arrayBuffer()
				);
				await prisma.object.update({
					where: { id },
					data: { pictureFile: filename }
				});
			}
		}

		return { newWaveForm };
	}
};
