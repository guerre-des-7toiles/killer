import prisma from '$lib/server/prisma.js';
import { error, json } from '@sveltejs/kit';

export async function POST({ params: { survivor, target }, locals: {user} }) {
	if (!user?.admin) throw error(403, { message: "Tu n'es pas administrateur·ice !" });
	await prisma.user.update({
		where: {
			id: survivor
		},
		data: {
			wantsToKill: {
				connect: {
					id: target
				}
			}
		}
	});
	return json({ status: 'ok' });
}
