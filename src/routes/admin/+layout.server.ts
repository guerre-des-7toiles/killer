import { currentWave } from '$lib/server/db.js';
import prisma from '$lib/server/prisma';
import { error } from '@sveltejs/kit';

export async function load({ locals }) {
	if (!locals.user?.admin) {
		error(403, { message: "Tu n'es pas administrateur·ice !" });
	}

	const wave = await currentWave({
		immunityObjects: true
	});

	return {
		wave,
		unassignedSurvivorsCount: prisma.user.count({
			where: {
				dead: false,
				wantsToKill: {
					none: {}
				},
				wantedBy: {
					none: {}
				}
			}
		}),
		unverifiedProofsCount: prisma.kill.count({
			where: {
				verdict: null,
				...(wave ? { waveId: wave.id } : {})
			}
		})
	};
}
