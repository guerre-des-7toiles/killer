import { ChurrosClient } from '$lib/graphql.js';
import { lucia } from '$lib/server/lucia';
import { redirect } from '@sveltejs/kit';
import { gql } from 'graphql-request';

export async function GET({ locals }) {
	const sessionId = locals.session?.id;
	const churros = ChurrosClient(locals.session?.churrosToken ?? '');
	await churros.request(gql`
		mutation {
			logout
		}
	`);
	if (sessionId) await lucia.invalidateSession(sessionId);
	redirect(302, '/');
}
