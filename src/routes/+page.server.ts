import * as schemas from '$lib/schemas.js';
import { currentWave } from '$lib/server/db.js';
import { saveToStorage } from '$lib/server/files.js';
import prisma from '$lib/server/prisma.js';
import { error, fail, redirect } from '@sveltejs/kit';
import path from 'node:path';
import { superValidate } from 'sveltekit-superforms/server';

export async function load({ locals }) {
	if (!locals.user) {
		redirect(307, '/oauth/login');
	}
	const wave = await currentWave({ immunityObjects: true });
	const submitKill = await superValidate(schemas.submitKillProof);
	const targetVictim = await prisma.user.findFirst({
		where: {
			dead: false,
			wantedBy: {
				some: {
					id: locals.user?.id
				}
			}
		}
	});

	submitKill.data.murderer = locals.user.id;
	submitKill.data.victim = targetVictim?.id ?? '';

	return {
		submitKill: submitKill,
		wavesSurvived: prisma.wave.findMany({
			where: {
				kills: {
					none: {
						victimId: locals.user.id,
						verdict: true
					}
				}
			}
		}),
		me: locals.user,
		targetVictim,
		currentWave: wave,
		survivors: prisma.user.findMany({
			where: { dead: false }
		}),
		pendingProof:
			locals.user && wave
				? await prisma.kill.findFirst({
						where: {
							murdererId: locals.user.id,
							waveId: wave.id
						},
						include: {
							judge: true
						},
						orderBy: {
							submittedAt: 'desc'
						}
					})
				: null
	};
}

export const actions = {
	async submitProof({ request, locals }) {
		const formData = await request.formData();
		const form = await superValidate(formData, schemas.submitKillProof);
		if (form.data.murderer !== locals.user?.id && !locals.user?.admin) {
			error(403, {
				message: "Seuls les administrateur·ice·s peuvent déclarer quelqu'un d'autre comme meurtrier"
			});
		}

		if (!form.valid) {
			return fail(400, { submitKillForm: form });
		}

		const wave = await currentWave({});

		if (!wave) {
			error(400, {
				message: "Aucune vague n'est en cours"
			});
		}

		const proof = await prisma.kill.create({
			data: {
				proofFile: '',
				murdererId: form.data.murderer,
				victimId: form.data.victim,
				waveId: wave.id
			}
		});

		const photo = formData.get('photo') ?? '';
		if (photo instanceof File) {
			const filename = `proofs/${proof.id}${path.extname(photo.name)}`;
			const proofFile = await saveToStorage(filename, await photo.arrayBuffer());
			await prisma.kill.update({
				where: { id: proof.id },
				data: { proofFile }
			});
		}
	}
};
