import { ChurrosClient } from '$lib/graphql';
import { churros as churrosOauth, lucia } from '$lib/server/lucia.js';
import prisma from '$lib/server/prisma.js';
import { error, fail, redirect } from '@sveltejs/kit';
import { OAuth2RequestError } from 'arctic';
import { isAfter, isBefore } from 'date-fns';
import { gql } from 'graphql-request';
import path from 'node:path/posix';

export async function GET({ url, cookies }) {
	const stateFromCookie = cookies.get('churros_oauth_state');

	const stateFromURL = url.searchParams.get('state');
	const code = url.searchParams.get('code');

	if (!stateFromCookie || !stateFromURL || !code || stateFromCookie !== stateFromURL) {
		error(400);
	}

	const { accessToken } = await churrosOauth.validateAuthorizationCode(code).catch((err) => {
		console.error(err);
		if (err instanceof OAuth2RequestError) {
			return fail(400);
		}
		error(500);
	});

	const churros = ChurrosClient(accessToken);

	const {
		me: {
			id: churrosId,
			email,
			admin: churrosAdmin,
			fullName,
			groups: memberships,
			uid: churrosUid,
			pictureFile
		}
	} = (await churros.request(gql`
		query {
			me {
				id
				email
				uid
				admin
				fullName
				pictureFile
				groups {
					president
					vicePresident
					treasurer
					secretary
					group {
						uid
					}
				}
			}
		}
	`)) as {
		me: {
			id: string;
			email: string;
			uid: string;
			pictureFile: string;
			admin: boolean;
			fullName: string;
			groups: Array<{
				president: boolean;
				vicePresident: boolean;
				treasurer: boolean;

				group: { uid: string };
			}>;
		};
	};

	const admin =
		churrosAdmin ||
		memberships.some(
			(membership) =>
				Object.values(membership).some((role) => typeof role === 'boolean' && role) &&
				membership.group.uid === 'bds-n7'
		);

	const existingUser = await prisma.user.findUnique({
		where: { churrosId }
	});

	if (existingUser) {
		const session = await lucia.createSession(existingUser.id, {
			churrosToken: accessToken
		});
		const sessionCookie = lucia.createSessionCookie(session.id);
		cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '/',
			...sessionCookie.attributes
		});
		redirect(302, '/');
	}

	const settings = await prisma.settings.findUniqueOrThrow({ where: { id: '_' } });

	function formatDateTimeForFrance(date: Date) {
		return new Intl.DateTimeFormat('fr-FR', {
			timeZone: 'Europe/Paris',
			dateStyle: 'medium',
			timeStyle: 'medium'
		}).format(date);
	}

	if (!admin && isBefore(new Date(), settings.inscriptionsOpenAt)) {
		error(403, {
			message: `Les inscriptions ne sont pas encore ouvertes. RDV le ${formatDateTimeForFrance(settings.inscriptionsOpenAt)}`
		});
	}

	if (!admin && settings.inscriptionsCloseAt && isAfter(new Date(), settings.inscriptionsCloseAt)) {
		error(403, {
			message: `Les inscriptions sont fermées. Déso!`
		});
	}

	const user = await prisma.user.create({
		data: {
			churrosId,
			email,
			admin,
			name: fullName,
			churrosUid,
			pictureURL: new URL(path.join('storage', pictureFile), 'https://churros.inpt.fr')
		}
	});
	const session = await lucia.createSession(user.id, {
		churrosToken: accessToken
	});
	const sessionCookie = lucia.createSessionCookie(session.id);
	cookies.set(sessionCookie.name, sessionCookie.value, {
		path: '/',
		...sessionCookie.attributes
	});
	redirect(302, '/');
}
