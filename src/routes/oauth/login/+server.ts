import { dev } from '$app/environment';
import { churros } from '$lib/server/lucia.js';
import { redirect } from '@sveltejs/kit';
import { generateState } from 'arctic';

export async function GET({ cookies }) {
	const state = generateState();
	const url = await churros.createAuthorizationURL(state);

	cookies.set('churros_oauth_state', state, {
		path: '/',
		httpOnly: true,
		secure: !dev,
		maxAge: 60 * 10
	});

	redirect(302, url);
}
