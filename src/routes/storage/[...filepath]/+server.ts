import { error, redirect } from '@sveltejs/kit';
import { ReadStream, createReadStream } from 'node:fs';
import path from 'node:path/posix';
import mime from 'mime';
import * as vercelBlob from '@vercel/blob';

export async function GET({ params: { filepath: pathParts } }) {
	const filepath = path.join(pathParts);

	if (process.env.ORIGIN?.includes('vercel.app')) {
		const blob = await vercelBlob
			.list({
				token: process.env.PRIVATE_PHOTOS_READ_WRITE_TOKEN
			})
			.then(({ blobs }) => blobs.find((blob) => blob.pathname === filepath));

		if (blob) redirect(302, blob.url);
		else error(404, { message: 'Not found' });
	} else {
		const storageDirectory = path.resolve(path.join(process.cwd(), 'storage'));

		const resolvedFilepath = path.resolve(path.join(storageDirectory, filepath));

		if (path.relative(storageDirectory, resolvedFilepath).startsWith('..')) {
			error(403, { message: 'Forbidden' });
		}

		const stream = createReadStream(resolvedFilepath);

		return new Response(await readStreamToReadableStream(stream), {
			headers: {
				'Content-Type': mime.getType(resolvedFilepath) ?? 'application/octet-stream'
			}
		});
	}
}

async function readStreamToReadableStream(stream: ReadStream) {
	return iteratorToStream(nodeStreamToIterator(stream));
}

/**
 * From https://github.com/MattMorgis/async-stream-generator
 */
async function* nodeStreamToIterator(stream) {
	for await (const chunk of stream) {
		yield chunk;
	}
}

/**
 * Taken from Next.js doc
 * https://nextjs.org/docs/app/building-your-application/routing/router-handlers#streaming
 * Itself taken from mozilla doc
 * https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream#convert_async_iterator_to_stream
 * @param {*} iterator
 * @returns {ReadableStream}
 */
function iteratorToStream(iterator) {
	return new ReadableStream({
		async pull(controller) {
			const { value, done } = await iterator.next();

			if (done) {
				controller.close();
			} else {
				controller.enqueue(new Uint8Array(value));
			}
		}
	});
}
