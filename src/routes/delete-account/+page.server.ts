import prisma from '$lib/server/prisma.js';
import { redirect } from '@sveltejs/kit';

export async function load({ locals }) {
	if (!locals.user) redirect(307, '/');
}

export const actions = {
	async default({ locals }) {
		await prisma.user.update({
			where: { id: locals.user.id },
			data: { dead: true }
		});
	}
};
