import { lucia } from '$lib/server/lucia';
import prisma from '$lib/server/prisma';
import { error } from '@sveltejs/kit';

export async function handle({ event, event: { cookies, locals, url }, resolve }) {
	const { session } = await lucia.validateSession(cookies.get('auth_session') ?? '');
	if (session) {
		locals.user = await prisma.user.findUnique({
			where: { id: session.userId }
		});
		locals.session = session;
	} else {
		locals.user = null;
		locals.session = null;
	}

	if (url.pathname.startsWith('/admin') && !locals.user?.admin) {
		error(403, { message: "Tu n'es pas administrateur·ice !" });
	}

	return resolve(event);
}
