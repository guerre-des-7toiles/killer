import type { OAuth2Provider } from 'arctic';
import { OAuth2Client } from 'oslo/oauth2';

const authorizeEndpoint = 'https://churros.inpt.fr/authorize';
const tokenEndpoint = 'https://churros.inpt.fr/token';

export interface ChurrosTokens {
	accessToken: string;
	expiresIn: number;
}

export class Churros implements OAuth2Provider {
	private client: OAuth2Client;
	private secret: string;

	constructor(clientId: string, clientSecret: string, redirectURI: string) {
		this.client = new OAuth2Client(clientId, authorizeEndpoint, tokenEndpoint, {
			redirectURI
		});
		this.secret = clientSecret;
	}

	public async createAuthorizationURL(state: string): Promise<URL> {
		return await this.client.createAuthorizationURL({ state });
	}

	public async validateAuthorizationCode(code: string): Promise<ChurrosTokens> {
		const result = await this.client.validateAuthorizationCode(code, {
			authenticateWith: 'http_basic_auth',
			credentials: this.secret
		});
		const tokens: ChurrosTokens = {
			accessToken: result.access_token,
			expiresIn: result.expires_in ?? 0
		};
		return tokens;
	}
}
