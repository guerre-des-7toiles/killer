import type { Prisma } from '@prisma/client';
import prisma from './prisma';

export async function currentWave<Inc extends Prisma.WaveInclude>(include: Inc) {
	return await prisma.wave.findFirst({
		include,
		where: {
			startsAt: {
				lte: new Date()
			},
			endsAt: {
				gte: new Date()
			}
		}
	});
}

export async function updateUserKilledStatus(userId: string) {
	await prisma.user.update({
		where: { id: userId },
		data: {
			dead:
				(await prisma.kill.count({
					where: {
						victimId: userId,
						verdict: true
					}
				})) > 0
		}
	});
}
