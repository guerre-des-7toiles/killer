import { Lucia } from 'lucia';
import { PrismaAdapter } from '@lucia-auth/adapter-prisma';
import prisma from './prisma';
import { dev } from '$app/environment';
import { Churros } from './oauth';
import {env as config} from '$env/dynamic/public';
import {env as secrets} from '$env/dynamic/private';

export const lucia = new Lucia(new PrismaAdapter(prisma.session, prisma.user), {
	sessionCookie: {
		attributes: {
			secure: !dev
		}
	},
	getUserAttributes: (attributes) => {
		return {
			email: attributes.email,
			churrosId: attributes.churrosId,
			admin: attributes.admin,
			name: attributes.name,
			dead: attributes.dead
		};
	},
	getSessionAttributes: (attributes) => {
		return {
			churrosToken: attributes.churrosToken
		};
	}
});

export const churros = new Churros(
	config.PUBLIC_CHURROS_CLIENT_ID,
	secrets.CHURROS_CLIENT_SECRET,
	new URL('/oauth/callback', secrets.ORIGIN).toString()
);
