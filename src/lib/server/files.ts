import { mkdir, writeFile } from 'node:fs/promises';
import path from 'node:path';
import { fileTypeFromBuffer } from 'file-type';
import mime from 'mime';
import * as vercelBlob from '@vercel/blob';

/**
 * Save a file to storage
 * @param filename Relative to the storage directory.
 * @param contents Contents of the file.
 * @returns The filepath, relative to the storage directory, with the extension possibly changed
 */
export async function saveToStorage(
	filename: string,
	contents: string | ArrayBuffer
): Promise<string> {
	const declaredExtension = path.extname(filename).slice(1);
	const filebase = path.basename(filename, path.extname(filename));

	const extension =
		typeof contents === 'string'
			? // if the file is a string, only use the original extension if it doesn't lie about the fact that the file is text.
				mime.getType(declaredExtension)?.includes('text/')
				? declaredExtension
				: 'txt'
			: // if the file is binary, detect the actual extension.
				await fileTypeFromBuffer(contents).then((typ) => typ?.ext ?? 'bin');

	const relativeToStorage = path.join(path.dirname(filename), `${filebase}.${extension}`);

	if (process.env.ORIGIN?.includes('vercel.app')) {
		await vercelBlob.put(relativeToStorage, contents, {
			access: 'public',
			token: process.env.PRIVATE_PHOTOS_READ_WRITE_TOKEN
		});
	} else {
		const fullPath = path.join('./storage', relativeToStorage);
		await mkdir(path.dirname(fullPath), { recursive: true });

		await writeFile(fullPath, typeof contents === 'string' ? contents : Buffer.from(contents));
	}

	return relativeToStorage;
}
