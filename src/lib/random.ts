export function randomElement<T>(elements: T[]): T {
    return elements[Math.random() * elements.length]
}

export function randomSort<T>(elements: T[]): T[] {
    return elements.sort(() => Math.random() - 0.5)
}
