import { format, isBefore, parse } from 'date-fns';
import { z } from 'zod';

export const assignTarget = z
	.object({
		firstUser: z.string().uuid(),
		otherUser: z.string().uuid()
	})
	.superRefine(({ firstUser, otherUser }, ctx) => {
		if (firstUser === otherUser) {
			ctx.addIssue({
				code: z.ZodIssueCode.custom,
				path: ['otherUser'],
				message: 'Les deux cibles ne peuvent pas être les mêmes'
			});
		}
	});

export const unassignTarget = z.object({
	user: z.string().uuid()
});

export const createWave = z
	.object({
		name: z.string().min(1).max(255),
		objectName: z.string(),
		// objectPicture: z
		// 	.custom<File>()
		// 	.optional()
		// 	.refine((f) => f instanceof File && f.size < 5 * 1024 * 1024, {
		// 		message: "L'image doit faire moins de 5 Mo"
		// 	}),
		timezone: z
			.string()
			.default('Europe/Paris')
			.refine(
				(tz) => {
					try {
						Intl.DateTimeFormat(undefined, { timeZone: tz });
						return true;
					} catch {
						return false;
					}
				},
				{
					message: 'Le fuseau horaire est invalide'
				}
			),
		date: z
			.string()
			.default(format(new Date(), 'yyyy-MM-dd'))
			.refine((d) => !isNaN(parse(d, 'yyyy-MM-dd', new Date()).getTime()), {
				message: 'La date doit être au format YYYY-MM-DD'
			}),
			// .refine(
			// 	(d) => {
			// 		const date = parse(d, 'yyyy-MM-dd', new Date());
			// 		return isFuture(date) || isToday(date);
			// 	},
			// 	{
			// 		message: 'La date doit être dans le futur ou aujourd’hui'
			// 	}
			// ),
		startTime: z
			.string()
			.default('00:00')
			.refine((t) => !isNaN(parse(t, 'HH:mm', new Date()).getTime()), {
				message: "L'heure doit être au format HH:mm"
			}),
		endTime: z
			.string()
			.default('23:59')
			.refine((t) => !isNaN(parse(t, 'HH:mm', new Date()).getTime()), {
				message: "L'heure doit être au format HH:mm"
			})
	})
	.superRefine(({ startTime, endTime }, ctx) => {
		const start = parse(startTime, 'HH:mm', new Date());
		const end = parse(endTime, 'HH:mm', new Date());
		if (isBefore(end, start))
			ctx.addIssue({
				code: z.ZodIssueCode.too_small,
				type: 'string',
				path: ['endTime'],
				message: "L'heure de fin doit être après l'heure de début",
				minimum: start.getTime(),
				inclusive: false
			});
	});

export const submitKillProof = z.object({
	murderer: z.string().uuid(),
	victim: z.string().uuid()
});

export const acceptKillProof = z.object({
	proof: z.string().uuid()
});

export const denyKillProof = z.object({
	proof: z.string().uuid()
});
