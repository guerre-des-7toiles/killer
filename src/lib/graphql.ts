export const endpoint = 'https://churros.inpt.fr/graphql';
import { GraphQLClient } from 'graphql-request';

export const ChurrosClient = (token: string) =>
	new GraphQLClient(endpoint, {
		headers: {
			Authorization: `Bearer ${token}`
		}
	});
