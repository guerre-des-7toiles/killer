import { formatDistanceToNow } from 'date-fns';
import fr from 'date-fns/locale/fr/index.js';
import { zonedTimeToUtc } from 'date-fns-tz';

export function formatTimeToNow(date: Date, { prefix = false }) {
	return formatDistanceToNow(date, { addSuffix: prefix, locale: fr });
}

export function createDateTime(date: string, time: string, timezone: string) {
	return zonedTimeToUtc(`${date}T${time}:00`, timezone);
}
