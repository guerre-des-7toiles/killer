/*
  Warnings:

  - Added the required column `churrosToken` to the `Session` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Session` ADD COLUMN `churrosToken` VARCHAR(191) NOT NULL;
