/*
  Warnings:

  - You are about to drop the column `verifiedById` on the `Kill` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `Kill` DROP FOREIGN KEY `Kill_verifiedById_fkey`;

-- AlterTable
ALTER TABLE `Kill` DROP COLUMN `verifiedById`,
    ADD COLUMN `judgeId` VARCHAR(191) NULL,
    MODIFY `verifiedAt` DATETIME(3) NULL;

-- AddForeignKey
ALTER TABLE `Kill` ADD CONSTRAINT `Kill_judgeId_fkey` FOREIGN KEY (`judgeId`) REFERENCES `User`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
