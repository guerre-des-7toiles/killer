/*
  Warnings:

  - Added the required column `proofFile` to the `Kill` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Kill` ADD COLUMN `proofFile` VARCHAR(191) NOT NULL,
    ADD COLUMN `verdict` BOOLEAN NULL;
