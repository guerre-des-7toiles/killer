/*
  Warnings:

  - A unique constraint covering the columns `[churrosUid]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `churrosUid` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `User` ADD COLUMN `churrosUid` VARCHAR(191) NOT NULL,
    ADD COLUMN `pictureURL` VARCHAR(191) NOT NULL DEFAULT '';

-- CreateIndex
CREATE UNIQUE INDEX `User_churrosUid_key` ON `User`(`churrosUid`);
