#!/usr/bin/env sh

cd /app
echo Applying migrations
npx prisma migrate deploy
echo Patching prisma client
file=$(ls ./build/server/chunks/prisma-*.js)
tempfile=$(mktemp)
echo "import * as path from 'path'" >> $tempfile
echo "const __filename = new URL('.', import.meta.url).pathname" >> $tempfile
echo "const __dirname = path.dirname(__filename)" >> $tempfile
cp $file copy
cat $tempfile copy > $file
tail $file
echo Spinning up the app
node ./build/index.js
